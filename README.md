Task Tracker
================

This application requires:

- Ruby 2.3.1
- Rails 5.0.0.1
- PostgreSQL

Setup
-----------

`bin/setup`

Run
-------------

`bin/rails s`

visit `http://localhost:3000`

The database will be filled from the `db/seeds.rb` file

Admin access:
email: "user.black@example.com"
password: "testPassword123"

All other users emails generates by Faker gem and could be found on `http://localhost:3000/users` page.
Passwords for all users the same: "testPassword123"
