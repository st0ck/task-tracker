class ChangeColumnInIssues < ActiveRecord::Migration[5.0]
  def up
    execute <<-SQL
      UPDATE issues
      SET title='Untitled'
      where title IS NULL
    SQL

    change_column :issues, :title, :string, null: false
  end

  def down
    change_column :issues, :title, :string, null: true
  end
end
