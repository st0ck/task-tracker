class ChangeRoleInUsers < ActiveRecord::Migration[5.0]
  def up
    execute <<-SQL
      UPDATE users
      SET role=0
      where role IS NULL
    SQL

    add_column :users, :role, :integer, null: false, default: 0
  end

  def down
    change_column :users, :role, :integer, null: true
  end
end
