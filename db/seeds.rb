User.destroy_all
User.create!({email: "user.black@example.com", password: "testPassword123", password_confirmation: "testPassword123", name: "User Black", role: "admin"})
9.times { User.create!(email: Faker::Internet.email, name: Faker::Name.name, password: "testPassword123", password_confirmation: "testPassword123") }

Issue.destroy_all
30.times { Issue.create!(title: Faker::Lorem.sentence, description: Faker::Lorem.paragraph) }

first_user_id = User.first.id
Issue.all.each do |issue|
  issue.participants = User.where(id: (rand(5) + 1).times.map { first_user_id + rand(User.count) })
  issue.participants.each { |user| issue.comments.create(user_id: user.id, content: Faker::Lorem.sentence) }
end
