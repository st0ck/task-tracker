require 'test_helper'

class CommentsControllerTest < ActionDispatch::IntegrationTest
  let(:admin) { create(:user, role: 'admin') }
  let(:comment) { create(:comment, user_id: admin.id) }
  let(:new_message) { 'new message' }
  let(:back) { 'http://example.com' }

  describe 'actions' do
    before do
      comment
      sign_in(admin)
    end

    test '#create' do
      params = { comment: { user_id: admin.id, issue_id: comment.issue.id, content: new_message } }
      assert_difference('Comment.count', 1) do
        post(comments_url, params: params, headers: { HTTP_REFERER: back })
      end
      comment = Comment.last
      comment.content.must_equal(new_message)

      assert_redirected_to back
    end

    test '#update' do
      patch(comment_url(comment), params: { comment: { content: new_message } })
      comment.reload
      comment.content.must_equal(new_message)

      assert_redirected_to issue_path(comment.issue)
    end

    test '#destroy' do
      assert_difference('Comment.count', -1) do
        delete(comment_url(comment), headers: { HTTP_REFERER: back })
      end

      assert_redirected_to back
    end
  end
end
