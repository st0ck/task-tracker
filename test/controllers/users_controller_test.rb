require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  let(:admin) { create(:user, role: 'admin') }
  let(:user) { create(:user) }
  let(:new_name) { 'new name' }
  let(:new_email) { 'its@new.email' }

  describe 'actions' do
    before do
      user
      sign_in(admin)
    end

    test '#index' do
      get users_url
      assert_response :success
    end

    test '#update' do
      patch user_url(user), params: { user: { name: new_name, email: new_email } }
      user.reload.name.must_equal(new_name)
      user.email.must_equal(new_email)
      assert_redirected_to users_path
    end

    test '#show' do
      get user_url(user)
      assert_response :success
    end

    test '#edit' do
      get edit_user_url(user)
      assert_response :success
    end

    test '#destroy' do
      assert_difference('User.count', -1) do
        delete user_url(user)
      end
      assert_redirected_to users_path
    end
  end
end
