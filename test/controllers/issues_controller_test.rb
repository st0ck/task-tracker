require 'test_helper'

class IssuesControllerTest < ActionDispatch::IntegrationTest
  let(:admin) { create(:user, role: 'admin') }
  let(:user) { create(:user) }
  let(:issue) { create(:issue) }
  let(:new_title) { 'new name' }
  let(:new_desc) { 'new description' }
  let(:new_label) { 'new label' }
  let(:new_state) { 'new_state' }
  let(:new_params) do
    {
      title: new_title,
      description: new_desc,
      label: new_label,
      state: new_state,
      participant_ids: [admin.id]
    }
  end

  describe 'actions' do
    before do
      issue
      sign_in(admin)
    end

    test '#index' do
      get issues_url
      assert_response :success
    end

    test '#new' do
      get new_issue_url
      assert_response :success
    end

    test '#create' do
      post(issues_url, params: { issue: new_params })
      issue = Issue.last
      issue.title.must_equal(new_title)
      issue.description.must_equal(new_desc)
      issue.label.must_equal(new_label)
      issue.state.must_equal(new_state.humanize)
      issue.participant_ids.must_equal([admin.id])

      assert_redirected_to issue_path(issue)
    end

    test '#update' do
      patch(issue_url(issue), params: { issue: new_params })
      issue.reload
      issue.title.must_equal(new_title)
      issue.description.must_equal(new_desc)
      issue.label.must_equal(new_label)
      issue.state.must_equal(new_state.humanize)
      issue.participant_ids.must_equal([admin.id])

      assert_redirected_to issue_path(issue)
    end

    test '#show' do
      get issue_url(issue)
      assert_response :success
    end

    test '#edit' do
      get edit_issue_url(issue)
      assert_response :success
    end

    test '#destroy' do
      assert_difference('Issue.count', -1) do
        delete issue_url(issue)
      end
      assert_redirected_to issues_path
    end
  end
end
