FactoryGirl.define do
  factory :user do
    name { Faker::Name.name }
    email { Faker::Internet.safe_email(name) }
    password Faker::Internet.password
    role { 'user' }
  end
end
