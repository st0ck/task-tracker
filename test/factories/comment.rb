FactoryGirl.define do
  factory :comment do
    user
    issue
    content { Faker::Lorem.sentence }
  end
end
