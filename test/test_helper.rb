ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

# TODO: fix it
# require 'minitest/autorun' causes double test execution
# more: https://github.com/blowmage/minitest-rails/issues/185
# here is temporal workaround ->
require 'minitest'
require 'minitest/spec'
require 'minitest/mock'
require 'minitest/hell' if ENV['MT_HELL']
# <- end workeround

require 'database_cleaner'

DatabaseCleaner.strategy = :transaction

module AroundEachTest
  def before_setup
    super
    DatabaseCleaner.clean
    DatabaseCleaner.start
  end
end

module ActiveSupport
  class TestCase
    include FactoryGirl::Syntax::Methods
    include AroundEachTest
    extend Minitest::Spec::DSL

    # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
    fixtures :all

    # Add more helper methods to be used by all tests here...
  end
end

module ActionDispatch
  class IntegrationTest
    include Devise::Test::IntegrationHelpers
  end
end
