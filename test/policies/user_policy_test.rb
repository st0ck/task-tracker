require 'test_helper'

class UserPolicyTest < ActiveSupport::TestCase
  let(:admin) { FactoryGirl.create(:user, role: 'admin') }
  let(:other_admin) { FactoryGirl.create(:user, role: 'admin') }
  let(:user) { FactoryGirl.create(:user) }
  let(:other_user) { FactoryGirl.create(:user) }

  def scope(user)
    UserPolicy::Scope.new(user, User).resolve
  end

  def authorized?(user, subject, action)
    UserPolicy.new(user, subject).send(action)
  end

  def permitted_attrs(user, subject)
    UserPolicy.new(user, subject).permitted_attributes
  end

  describe 'scope' do
    before do
      user
      admin
    end

    it { assert_equal(User.all, scope(admin)) }
    it { assert_equal(User.none, scope(user)) }
  end

  describe 'index?' do
    it { assert_equal(true, authorized?(admin, User, :index?)) }
    it { assert_equal(false, authorized?(user, User, :index?)) }
  end

  describe 'show?' do
    it { assert_equal(true, authorized?(admin, user, :show?)) }
    it { assert_equal(true, authorized?(user, user, :show?)) }
    it { assert_equal(false, authorized?(other_user, user, :show?)) }
  end

  describe 'update?' do
    it { assert_equal(true, authorized?(admin, user, :show?)) }
    it { assert_equal(true, authorized?(other_admin, admin, :show?)) }
    it { assert_equal(true, authorized?(user, user, :show?)) }
    it { assert_equal(false, authorized?(other_user, user, :show?)) }
  end

  describe 'destroy?' do
    it { assert_equal(true, authorized?(admin, user, :show?)) }
    it { assert_equal(true, authorized?(admin, admin, :show?)) }
    it { assert_equal(true, authorized?(other_admin, admin, :show?)) }
    it { assert_equal(true, authorized?(user, user, :show?)) }
    it { assert_equal(false, authorized?(other_user, user, :show?)) }
  end

  describe 'edit?' do
    it { assert_equal(true, authorized?(admin, user, :show?)) }
    it { assert_equal(true, authorized?(user, user, :show?)) }
    it { assert_equal(false, authorized?(other_user, user, :show?)) }
  end

  describe 'permitted_attributes' do
    it { assert_equal(%i(email name), permitted_attrs(user, user)) }

    it 'permit password' do
      assert_equal(%i(email name role password password_confirmation), permitted_attrs(admin, user))
    end
  end
end
