require 'test_helper'

class CommentPolicyTest < ActiveSupport::TestCase
  let(:admin) { FactoryGirl.create(:user, role: 'admin') }
  let(:user) { FactoryGirl.create(:user) }
  let(:issue) { FactoryGirl.create(:issue) }
  let(:comment_admin) { FactoryGirl.create(:comment, user_id: admin.id, issue_id: issue.id) }
  let(:comment_user) { FactoryGirl.create(:comment, user_id: user.id, issue_id: issue.id) }

  def authorized?(user, subject, action)
    CommentPolicy.new(user, subject).send(action)
  end

  def permitted_attrs(user, subject)
    CommentPolicy.new(user, subject).permitted_attributes
  end

  describe 'create?' do
    it { assert_equal(true, authorized?(admin, comment_admin, :create?)) }
    it { assert_equal(true, authorized?(user, comment_user, :create?)) }
  end

  describe 'update?' do
    describe 'when user updates own comment' do
      it { assert_equal(true, authorized?(admin, comment_admin, :update?)) }
      it { assert_equal(true, authorized?(user, comment_user, :update?)) }
    end

    describe 'when user updates other user comment' do
      it { assert_equal(false, authorized?(admin, comment_user, :update?)) }
      it { assert_equal(false, authorized?(user, comment_admin, :update?)) }
    end
  end

  describe 'edit?' do
    describe 'when user edits own comment' do
      it { assert_equal(true, authorized?(admin, comment_admin, :edit?)) }
      it { assert_equal(true, authorized?(user, comment_user, :edit?)) }
    end

    describe 'when user edits other user comment' do
      it { assert_equal(false, authorized?(admin, comment_user, :edit?)) }
      it { assert_equal(false, authorized?(user, comment_admin, :edit?)) }
    end
  end

  describe 'destroy?' do
    describe 'when user destroys own comment' do
      it { assert_equal(true, authorized?(admin, comment_admin, :destroy?)) }
      it { assert_equal(true, authorized?(user, comment_user, :destroy?)) }
    end

    describe 'when user destroys other user comment' do
      it { assert_equal(true, authorized?(admin, comment_user, :destroy?)) }
      it { assert_equal(false, authorized?(user, comment_admin, :destroy?)) }
    end
  end

  describe 'permitted_attributes' do
    let(:attrs) { %i(content user_id issue_id) }

    it { assert_equal(permitted_attrs(admin, comment_admin), attrs) }
    it { assert_equal(permitted_attrs(user, comment_user), attrs) }
  end
end
