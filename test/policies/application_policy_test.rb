require 'test_helper'

class ApplicationPolicyTest < ActiveSupport::TestCase
  class TestPolicy < ApplicationPolicy; end

  test 'should raise excetion if user is nil' do
    err = assert_raises Pundit::NotAuthorizedError do
      TestPolicy.new(nil, :test)
    end
    assert_equal 'must be logged in', err.message
  end

  test 'should initialize test policy if user is not blank' do
    assert_instance_of(TestPolicy, TestPolicy.new(OpenStruct.new, :test))
  end
end
