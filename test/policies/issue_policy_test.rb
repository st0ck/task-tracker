require 'test_helper'

class IssuePolicyTest < ActiveSupport::TestCase
  let(:admin) { FactoryGirl.create(:user, role: 'admin') }
  let(:user) { FactoryGirl.create(:user) }
  let(:issue) { FactoryGirl.create(:issue) }

  def scope(user)
    IssuePolicy::Scope.new(user, Issue).resolve
  end

  def authorized?(user, subject, action)
    IssuePolicy.new(user, subject).send(action)
  end

  def permitted_attrs(user, subject)
    IssuePolicy.new(user, subject).permitted_attributes
  end

  describe 'scope' do
    before do
      issue
    end

    it { assert_equal(Issue.all, scope(admin)) }
    it { assert_equal(Issue.all, scope(user)) }
  end

  describe 'index?' do
    it { assert_equal(true, authorized?(admin, Issue, :index?)) }
    it { assert_equal(true, authorized?(user, Issue, :index?)) }
  end

  describe 'show?' do
    it { assert_equal(true, authorized?(admin, issue, :show?)) }
    it { assert_equal(true, authorized?(user, issue, :show?)) }
  end

  describe 'new?' do
    it { assert_equal(true, authorized?(admin, issue, :new?)) }
    it { assert_equal(true, authorized?(user, issue, :new?)) }
  end

  describe 'create?' do
    it { assert_equal(true, authorized?(admin, issue, :create?)) }
    it { assert_equal(true, authorized?(user, issue, :create?)) }
  end

  describe 'update?' do
    describe 'when user updates owned issue' do
      before do
        issue.participants = [admin, user]
      end

      it { assert_equal(true, authorized?(admin, issue, :update?)) }
      it { assert_equal(true, authorized?(user, issue, :update?)) }
    end

    describe 'when user updates not participated issue' do
      it { assert_equal(true, authorized?(admin, issue, :update?)) }
      it { assert_equal(false, authorized?(user, issue, :update?)) }
    end
  end

  describe 'destroy?' do
    it { assert_equal(true, authorized?(admin, issue, :destroy?)) }
    it { assert_equal(false, authorized?(user, issue, :destroy?)) }
  end

  describe 'edit?' do
    describe 'when user updates owned issue' do
      before do
        issue.participants = [admin, user]
      end

      it { assert_equal(true, authorized?(admin, issue, :edit?)) }
      it { assert_equal(true, authorized?(user, issue, :edit?)) }
    end

    describe 'when user updates not participated issue' do
      it { assert_equal(true, authorized?(admin, issue, :edit?)) }
      it { assert_equal(false, authorized?(user, issue, :edit?)) }
    end
  end

  describe 'permitted_attributes' do
    let(:attrs) { [:title, :description, :state, :label, participant_ids: []] }
    it { assert_equal(permitted_attrs(admin, issue), attrs) }
    it { assert_equal(permitted_attrs(user, issue), attrs) }
  end
end
