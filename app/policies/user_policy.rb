class UserPolicy < ApplicationPolicy
  attr_reader :user, :record

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user  = user
      @scope = scope
    end

    def resolve
      if user.admin?
        scope.all
      else
        scope.none
      end
    end
  end

  def index?
    user.admin?
  end

  def show?
    user.admin? || user == record
  end

  def update?
    user.admin? || user == record
  end

  def destroy?
    return false if user == record
    user.admin?
  end

  def edit?
    user.admin? || user == record
  end

  def permitted_attributes
    base_params = %i(email name)
    return base_params unless user.admin?
    base_params + %i(role password password_confirmation)
  end
end
