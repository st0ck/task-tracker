# Comment policies
class CommentPolicy < ApplicationPolicy
  attr_reader :user, :record

  def create?
    user.present?
  end

  def update?
    record.user_id == user.id
  end

  def edit?
    record.user_id == user.id
  end

  def destroy?
    return true if user.admin?
    record.user_id == user.id
  end

  def permitted_attributes
    [:content, :user_id, :issue_id]
  end
end
