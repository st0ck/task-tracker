class IssuePolicy < ApplicationPolicy
  attr_reader :user, :record

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user  = user
      @scope = scope
    end

    def resolve
      scope.all
    end
  end

  def index?
    user.present?
  end

  def show?
    user.present?
  end

  def create?
    user.present?
  end

  def update?
    return true if user.admin?
    record.participant_ids.include?(user.id)
  end

  def edit?
    update?
  end

  def destroy?
    user.admin?
  end

  def new?
    user.present?
  end

  def permitted_attributes
    [:title, :description, :state, :label, participant_ids: []]
  end
end
