class Issue < ApplicationRecord
  STATES = { new: 0, in_progress: 1, suspended: 2, on_review: 3, done: 4, closed: 5 }.freeze

  has_many :comments, -> { order(created_at: :asc) }
  has_and_belongs_to_many :participants,
                          join_table: 'issues_users',
                          class_name: User,
                          association_foreign_key: :user_id,
                          primary_key: :issue_id

  validates_presence_of :title

  accepts_nested_attributes_for :participants,
                                allow_destroy: true,
                                reject_if: -> (record) { record[:id].nil? }

  def state
    read_attribute(:state).humanize
  end
end
