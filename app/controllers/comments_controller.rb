class CommentsController < AuthorizationController
  before_action :find_comment, only: %i(edit update destroy)

  def create
    authorize :comment
    @comment = Comment.new(permitted_attributes(Comment))
    if @comment.save
      redirect_back(fallback_location: root_path, notice: 'Comment created.')
    else
      flash[:alert] = @comment.errors.full_messages.join('<br>').html_safe
      redirect_back(fallback_location: root_path)
    end
  end

  def update
    if @comment.update(permitted_attributes(@comment))
      redirect_to @comment.issue, notice: 'Comment updated.'
    else
      flash[:alert] = @comment.errors.full_messages.join('<br>').html_safe
      redirect_back(fallback_location: root_path)
    end
  end

  def destroy
    @comment.destroy
    redirect_back(fallback_location: root_path, notice: 'Comment deleted.')
  end

  private

  def find_comment
    @comment = Comment.find(params[:id])
    authorize @comment
  end
end
