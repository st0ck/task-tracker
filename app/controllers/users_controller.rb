class UsersController < AuthorizationController
  before_action :find_user, only: %i(update destroy show edit)

  def index
    authorize User
    @users = policy_scope(User)
  end

  def update
    if @user.update_attributes(permitted_attributes(@user))
      redirect_to users_path, notice: 'User updated.'
    else
      flash[:alert] = @user.errors.full_messages.join('<br>').html_safe
      redirect_to users_path
    end
  end

  def destroy
    @user.destroy
    redirect_to users_path, notice: 'User deleted.'
  end

  private

  def find_user
    @user = User.find(params[:id])
    authorize @user
  end

  def sanitize_params
    if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
      params.delete[:password]
      params.delete[:password_confirmation]
    end
  end
end
