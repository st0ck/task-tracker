class IssuesController < AuthorizationController
  before_action :find_issue, only: %i(update show edit destroy)

  def index
    authorize Issue
    @issues = policy_scope(Issue).order('id desc').page(params[:page])
  end

  def new
    @issue = Issue.new
    @issue.participant_ids << current_user.id unless current_user.admin?
    authorize(@issue)
  end

  def create
    authorize :issue
    @issue = Issue.new(permitted_attributes(Issue))
    if @issue.save
      redirect_to @issue, notice: 'Issue created.'
    else
      flash[:alert] = @issue.errors.full_messages.join('<br>').html_safe
      redirect_back(fallback_location: root_path)
    end
  end

  def update
    if @issue.update(permitted_attributes(@issue))
      redirect_to @issue, notice: 'Issue updated.'
    else
      flash[:alert] = @issue.errors.full_messages.join('<br>').html_safe
      redirect_back(fallback_location: root_path)
    end
  end

  def destroy
    @issue.destroy
    redirect_to issues_path, notice: 'Issue deleted.'
  end

  private

  def find_issue
    @issue = Issue.find(params[:id])
    authorize @issue
  end
end
