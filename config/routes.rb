Rails.application.routes.draw do
  root to: 'issues#index'
  devise_for :users
  resources :users
  resources :issues
  resources :comments, only: %i(create edit update destroy)
end
